from cv2 import GaussianBlur, threshold, THRESH_BINARY, THRESH_OTSU
from numpy import ndarray


def otsus_binarization(image: ndarray) -> (float, ndarray):
    """
    Optimised for text recognition
    https://opencv24-python-tutorials.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_thresholding/py_thresholding.html#otsus-binarization
    """
    blur = GaussianBlur(image, (5, 5), 0)
    return threshold(blur, 0, 255, THRESH_BINARY + THRESH_OTSU)
