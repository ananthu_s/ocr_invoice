from PIL.Image import fromarray
from numpy import ndarray
from cv2 import boundingRect, resize, dilate, erode, getStructuringElement, MORPH_RECT, BORDER_CONSTANT, INTER_CUBIC, \
    copyMakeBorder
from pytesseract.pytesseract import image_to_string


def crop_image_cell(image: ndarray, cell: tuple[int, ...]) -> ndarray:
    """
    returns a new image cut from the `image` with bounding box `cell`.
    """
    return image[cell[1]:cell[1] + cell[3], cell[0]:cell[0] + cell[2]]


def cut_cells(contours: list[ndarray], original_image: ndarray) -> list[ndarray]:
    return [crop_image_cell(original_image, boundingRect(contour)) for contour in contours]


def cell_optimise_for_scan(image: ndarray) -> ndarray:
    kernel = getStructuringElement(MORPH_RECT, (2, 1))
    border = copyMakeBorder(image, 2, 2, 2, 2, BORDER_CONSTANT, value=[255, 255])
    resizing = resize(border, None, fx=2, fy=2, interpolation=INTER_CUBIC)
    dilation = dilate(resizing, kernel, iterations=1)
    erosion = erode(dilation, kernel, iterations=1)
    return erosion


def cell_line_to_text(image: ndarray) -> list[str]:
    pillow_image = fromarray(image)
    return image_to_string(pillow_image, config='--psm 6')
