from numpy import ndarray
from cv2 import addWeighted, dilate, erode, findContours, getStructuringElement, MORPH_RECT, RETR_TREE, \
    CHAIN_APPROX_SIMPLE, threshold, THRESH_BINARY, boundingRect

from lib.image.color_space import invert
from lib.image.otsu_binarization import otsus_binarization


def detect_lines(image: ndarray, seed: int) -> ndarray:
    """
    returns an image with detected horizontal and vertical lines
    """
    eroded = erode(image, seed, iterations=3)
    return dilate(eroded, seed, iterations=3)


def detect_boxes(image: ndarray) -> ndarray:
    """
    draws the boxes to match table from an inverted image
    """
    # 1/40 of total width
    kernel_length = len(image) // 40

    kernel_vertical = getStructuringElement(MORPH_RECT, (1, kernel_length))
    kernel_horizontal = getStructuringElement(MORPH_RECT, (kernel_length, 1))
    kernel_double_box = getStructuringElement(MORPH_RECT, (2, 2))

    horizontal_lines = detect_lines(image, kernel_horizontal)
    vertical_lines = detect_lines(image, kernel_vertical)

    both_lines = addWeighted(vertical_lines, 0.5, horizontal_lines, 0.5, 0.0)
    eroded = erode(~both_lines, kernel_double_box, iterations=3)
    _, box_threshold = threshold(eroded, 128, 255, THRESH_BINARY)
    return box_threshold


def slope(point_one, point_two) -> float:
    """
     returns the slope of two points.
    """
    return (point_two[1] - point_one[1]) / (point_two[0] - point_one[0])


def check_if_top_line_in_same_line(rectangle_one, rectangle_two, tolerance=0.01) -> bool:
    """
    checks if two points are on the same line based on their slope (within a tolerance).
    """
    one_x, one_y, one_w, one_h = rectangle_one
    two_x, two_y, two_w, two_h = rectangle_two
    point_a = (one_x, one_y)
    point_b = (one_x + one_w, one_y)
    point_c = (two_x, two_y)

    return (abs(slope(point_a, point_b) - slope(point_b, point_c))) < tolerance


def check_if_boxes_inside(outer, inner, tolerance=0.01):
    """
    checks if a box is inside the other box.
    used to ensure `table` box and boundary boxes are not included.
    """
    inner_x, inner_y, inner_w, inner_h = inner
    outer_x, outer_y, outer_w, outer_h = outer

    if (outer_x - inner_x < tolerance) and \
            (outer_y - inner_y < tolerance) and \
            ((inner_x + inner_w) - (outer_x + outer_w) < tolerance) and \
            ((inner_y + inner_h) - (outer_y + outer_h) < tolerance):
        return True

    return False


def filter_contours(contours):
    """
    removes `table` boxes and unnecessary boxes.
    data cells only returned.
    """
    boxes = []
    for contour in contours:
        rect_outer = boundingRect(contour)

        # skip if rect_tuple is has boxes inside it.
        if any(check_if_boxes_inside(rect_outer, boundingRect(inner_box)) for inner_box in boxes):
            continue

        to_remove = []
        # remove a box if it has boxes inside it from `boxes`.
        for index, box in enumerate(boxes):
            if check_if_boxes_inside(boundingRect(box), rect_outer):
                to_remove.append(index)

        if len(to_remove) > 0:
            boxes = [box for index, box in enumerate(boxes) if (index not in to_remove)]

        boxes.append(contour)

    return boxes


def box_contours(image: ndarray) -> tuple[ndarray]:
    """
    contours to find points.
    """
    contours, _ = findContours(image, RETR_TREE, CHAIN_APPROX_SIMPLE)
    return contours


def sort_contours_vertical(contours: tuple[ndarray]) -> tuple[ndarray]:
    """
    sorts bounding boxes list
    """
    # bounding_boxes = get_bounding_boxes(contours)

    return tuple(sorted(contours, key=lambda contour: boundingRect(contour)[1]))
    # return zip(*sorted(zip(contours, bounding_boxes),
    #                    key=lambda b: b[1][1]))


def sort_contour_columns(contours):
    contours.sort(key=lambda col: boundingRect(col)[0])
    return contours


def fragmented_contours_to_rows(contours: tuple[ndarray]) -> tuple[ndarray, ...]:
    """
    creates rows of bounding boxes based on if the top left point lies on the same line.
    (each row is sorted left-to-right)
    """
    columns = []

    # for duplicate check and skipping
    box_index_map = {}
    # todo: simple sorting, if two boxes overlap it doesn't handle it.

    for index, contour in enumerate(contours):
        if index in box_index_map:
            continue

        same_column = [contour]
        for index_other, contour_other in enumerate(contours):
            if index_other == index:
                continue

            if index_other in box_index_map:
                continue

            if check_if_top_line_in_same_line(boundingRect(contour), boundingRect(contour_other)):
                same_column.append(contour_other)
                box_index_map[index_other] = True

        if len(same_column) > 0:
            columns.append(sort_contour_columns(same_column))

    return columns


def get_contours_of_table(image: ndarray) -> tuple[list[ndarray], list[ndarray], list[ndarray]]:
    _, otsu = otsus_binarization(image)
    image_inverted = invert(otsu)
    detected_boxes = detect_boxes(image_inverted)
    detected_box_contours = box_contours(detected_boxes)
    sorted_detected_box_contours = sort_contours_vertical(detected_box_contours)
    filtered_detected_boxes = filter_contours(sorted_detected_box_contours)
    return fragmented_contours_to_rows(filtered_detected_boxes)
