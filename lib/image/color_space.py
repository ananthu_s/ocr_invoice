from numpy import ndarray
from cv2 import bitwise_not


def invert(image: ndarray) -> ndarray:
    """
    inverts black / white.
    """
    return bitwise_not(image)

