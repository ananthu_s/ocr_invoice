from numpy import ndarray
from cv2 import threshold, findContours, RETR_EXTERNAL, CHAIN_APPROX_NONE, dilate, THRESH_BINARY, bitwise_and, \
    THRESH_BINARY_INV, getStructuringElement, MORPH_CROSS, boundingRect, MORPH_RECT

from lib.image.cell import cut_cells, cell_line_to_text, cell_optimise_for_scan, crop_image_cell
from lib.image.color_space import invert


def get_body_rows(body: ndarray) -> list[ndarray]:
    """
    individual rows for each body item
    """
    _, mask = threshold(body, 180, 255, THRESH_BINARY)
    image_final = bitwise_and(body, body, mask=mask)
    _, new_img = threshold(image_final, 180, 255, THRESH_BINARY_INV)
    kernel = getStructuringElement(MORPH_CROSS, (2, 2))
    dilated = dilate(new_img, kernel, iterations=9)
    contours, _ = findContours(dilated, RETR_EXTERNAL,
                               CHAIN_APPROX_NONE)
    significant_contours = []

    for contour in contours:
        x, y, width, height = boundingRect(contour)

        if width < 35 and height < 35:
            continue

        significant_contours.append(contour)

    # sort vertically
    sorted_contours = sorted(significant_contours, key=lambda significant_contour: boundingRect(significant_contour)[1])

    return sorted_contours


def identify_possible_column_rows(column_image: ndarray, width_ratio=0.02, height_ratio=0.02) -> list[ndarray]:
    """
    returns cutouts of possible text from column.
    (rows in a column)
    """
    column_image_invert = invert(column_image)
    # .01 and .08 % of total width of total width (as table size is relatively stable)
    kernel_width = int(width_ratio * column_image_invert.shape[1])
    kernel_height = int(height_ratio * column_image_invert.shape[0])

    # kernel to join texts
    kernel = getStructuringElement(MORPH_RECT, (kernel_height, kernel_width))
    dilated = dilate(column_image_invert, kernel, iterations=1)
    possible_text_contours, _ = findContours(dilated, RETR_EXTERNAL, CHAIN_APPROX_NONE)

    # sort contours
    possible_text_contours = sorted(possible_text_contours, key=lambda contour: boundingRect(contour)[1])

    contour_cut_outs = [crop_image_cell(column_image_invert, boundingRect(contour)) for contour in
                        possible_text_contours]

    # filter cutouts
    contour_cut_outs = [cutout for cutout in contour_cut_outs if cutout.shape[0] >= 20 and cutout.shape[1] >= 20]
    return contour_cut_outs


def cut_columns_from_body(contours: list[ndarray], image: ndarray) -> list[list[ndarray]]:
    """
    returns cut-outs of each row column.
    (sorted since `contours` are sorted.)
    """
    columns = cut_cells(contours, image)
    return [identify_possible_column_rows(column) for column in columns]
