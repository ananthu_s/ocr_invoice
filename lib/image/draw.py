from numpy import ndarray
from cv2 import boundingRect


def rectangle_points_from_contours(contour: ndarray, padding=0) -> tuple[tuple[int, int], tuple[int, int]]:
    (x, y, w, h) = boundingRect(contour)

    x_one = x - padding
    y_one = y - padding
    if x_one < 0:
        x_one = 0

    if y_one < 0:
        y_one = 0

    return (x_one, y_one), (x + w + padding, y + h + padding)
