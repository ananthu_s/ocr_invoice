from numpy import ndarray

from lib.image.cell import cut_cells, cell_line_to_text, cell_optimise_for_scan


def parse_header(contours: list[ndarray], image: ndarray):
    """
    returns string list of header values
    """
    headers_cut = cut_cells(contours, image)
    return [cell_line_to_text(cell_optimise_for_scan(header_cut)) for header_cut in headers_cut]
