from numpy import zeros, ndarray
from cv2 import drawContours, bitwise_not, bitwise_and, imread, IMREAD_UNCHANGED


def remove_contours_from_image(contours: list[ndarray], image: ndarray) -> ndarray:
    image_mask = zeros(image.shape[:2], dtype=image.dtype)
    for contour in contours:
        drawContours(image_mask, [contour], 0, (255), -1)
    reverse_mask = bitwise_not(image_mask)
    return bitwise_and(image, image, mask=reverse_mask)


def read_image(file_path: str):
    return imread(file_path, IMREAD_UNCHANGED)
