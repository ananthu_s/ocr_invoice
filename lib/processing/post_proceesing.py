from lib.processing.column_type import invoice_number_regex


def process_invoice_title(title_strings: str):
    """
    gets the invoice number and name from top row.
    """
    number = invoice_number_regex.findall(title_strings)[0]

    return {
        'invoice_number': number,
        'name': title_strings.removesuffix(number).strip()
    }


def process_invoice_sub_titles(title_strings: str):
    small_caps = title_strings.lower()

    code_index = small_caps.index('code:')
    name_index = small_caps.index('name:')
    date_index = small_caps.index('date:')

    vendor_code = small_caps[code_index + 5:name_index].strip()
    name = small_caps[name_index + 5:date_index].strip()
    date = small_caps[date_index + 5:].strip()

    return {
        'vendor_code': vendor_code,
        'name': name,
        'check_date': date,
    }
