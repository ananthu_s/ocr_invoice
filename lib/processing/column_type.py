import re
from enum import Enum


class ColumnType(Enum):
    string = 'string'
    date = 'date'
    currency = 'currency'


date_regex = re.compile(r".*(\d{2}\/\d{2}\/\d{4}).*")
currency_regex = re.compile(r".*(\$[\.\,\d]*).*")
text_regex = re.compile(r"[a-zA-Z0-9]+")
non_alphanumeric_regex = re.compile(r"[^A-Za-z0-9 ]+")
invoice_number_regex = re.compile(r"\d+$")
