from typing import Pattern

from PIL.Image import fromarray
from numpy import ndarray
from pytesseract import image_to_string

from lib.image.cell import cell_optimise_for_scan, cut_cells
from lib.image.image import read_image, remove_contours_from_image
from lib.image.line_detection import get_contours_of_table
from lib.image.parse_body import cut_columns_from_body
from lib.processing.column_type import text_regex, date_regex, currency_regex, ColumnType, non_alphanumeric_regex


def tesseract_wrapper(image: ndarray, config: str = None):
    pillow_image = fromarray(image)
    return image_to_string(pillow_image, config=config)


def line_image_to_string(image: ndarray) -> str:
    return tesseract_wrapper(image, config='--psm 7')


def multi_line_image_to_string_list(image: ndarray) -> str:
    return tesseract_wrapper(image, config='--psm 4')


def safe_return_match(rx: Pattern[str], image: ndarray):
    raw_string = line_image_to_string(image)
    raw_string = raw_string.strip()

    if not raw_string:
        return None

    matches = rx.match(raw_string)

    if not matches:
        return None

    return matches[0]


def parse_date_image(image: ndarray) -> str | None:
    return safe_return_match(date_regex, image)


def parse_text_image(image: ndarray) -> str | None:
    return safe_return_match(text_regex, image)


def parse_currency_image(image: ndarray) -> str | None:
    return safe_return_match(currency_regex, image)


column_parser_method = {
    ColumnType.string: parse_text_image,
    ColumnType.date: parse_date_image,
    ColumnType.currency: parse_currency_image,
}


def filter_out_none(unfiltered: list[str]) -> list[str]:
    return list(filter(lambda item: item is not None, unfiltered))


def parse_list(column_type: ColumnType, rows: list[ndarray]):
    parser = column_parser_method[column_type]
    column_strings = [parser(image) for image in rows]
    column_strings = filter_out_none(column_strings)
    return column_strings


def parse_columns(expected_columns: list[ColumnType], columns: list[list[ndarray]]) -> list[list[str]]:
    parsed = []

    for (column_type, column) in zip(expected_columns, columns):
        parsed.append(parse_list(column_type, column))

    return parsed


def parse_headers(header_cutouts: list[ndarray]) -> list[str]:
    parsed_columns = [line_image_to_string(header) for header in header_cutouts]
    return [non_alphanumeric_regex.sub('', parsed_column).strip() for parsed_column in parsed_columns]


def parse_footer(footer_cutouts: list[ndarray]) -> list[str]:
    parsed_footers = [parse_currency_image(footer_cutout) for footer_cutout in footer_cutouts]
    return filter_out_none(parsed_footers)


def parse_rest_of_invoice(rest_cutout: ndarray) -> list[str]:
    scanned = multi_line_image_to_string_list(cell_optimise_for_scan(rest_cutout))
    scanned = scanned.split('\n')
    return list(filter(lambda item: item, scanned))


def ocr(file_path: str, expected_body_columns: list[ColumnType]):
    image = read_image(file_path)

    header, body, footer = get_contours_of_table(image)

    # header
    header_cutouts = cut_cells(header, image)
    header_parsed = parse_headers(header_cutouts)

    # body
    body_cutouts = cut_columns_from_body(body, image)
    body_parsed = parse_columns(expected_body_columns, body_cutouts)

    # footer
    footer_cutouts = cut_cells(footer, image)
    parsed_footer = parse_footer(footer_cutouts)

    # rest
    rest_of_invoice_cutout = remove_contours_from_image(header + footer + body, image)
    rest_of_invoice = parse_rest_of_invoice(rest_of_invoice_cutout)

    return {
        'header': header_parsed,
        'body': body_parsed,
        'footer': parsed_footer,
        'rest': rest_of_invoice,
    }
