from os.path import join
from os import getcwd


def path_join(path_string: str, *path_strings: str):
    return join(getcwd(), path_string, *path_strings)
