import json
from os import listdir
from os.path import isfile, join, basename, splitext

from lib.processing.column_type import ColumnType
from lib.processing.ocr import ocr
from lib.processing.post_proceesing import process_invoice_title, process_invoice_sub_titles
from lib.relative import path_join

if __name__ == '__main__':
    default_expected_body_columns = [ColumnType.string, ColumnType.date, ColumnType.currency, ColumnType.currency,
                                     ColumnType.currency, ColumnType.currency, ColumnType.currency]

    files_dir = path_join("test_files")
    for path_name in listdir(files_dir):
        full_path = join(files_dir, path_name)
        if not isfile(full_path):
            continue

        ocr_result = ocr(full_path, default_expected_body_columns)
        title = process_invoice_title(ocr_result['rest'][0])
        subtitle = process_invoice_sub_titles(ocr_result['rest'][1])
        ocr_result['rest'] = {
            'title': title,
            'subtitle': subtitle
        }

        with open(join(files_dir, splitext(basename(path_name))[0] + ".json"), 'w') as output:
            json.dump(ocr_result, output)
